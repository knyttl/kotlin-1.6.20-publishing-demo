plugins {
    id("org.jetbrains.kotlin.jvm") version "1.6.20"
    application
    id("maven-publish")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}

// build a repositories-testJar.jar with test classes for services tests
tasks.register<Jar>("testJar") {
    archiveClassifier.set("testJar")
    from(sourceSets["test"].output)
}

// the testJar must be built within the build phase
tasks.build {
    dependsOn(tasks.withType<Jar>())
}

// needed for publishing plugin to be aware of the testJar
configurations.create("testJar") {
    extendsFrom(configurations["testImplementation"])
}

artifacts {
    add("testJar", tasks.named<Jar>("testJar"))
}

val testArtifact = artifacts.add("testJar", tasks.named<Jar>("testJar"))

configure<PublishingExtension> {
    publications {
        create<MavenPublication>(project.name) {
            groupId = "demo"
            artifactId = project.name.toLowerCase().replace(":", "-")
            version = "ci"
            from(components["kotlin"])
        }

        create<MavenPublication>(project.name + "-" + testArtifact.name) {
            groupId = "demo"
            artifactId = project.name.toLowerCase().replace(":", "-")
            version = "ci"
            artifact(testArtifact)
        }
    }

    repositories {
        maven {
            url = uri("file://${rootProject.rootDir}/m2")
        }
    }
}


